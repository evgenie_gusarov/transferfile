import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Scanner;

public class TransferFile {
    private static final String WHERE_TO_MOVE = "введите путь откуда нужно скопировать файлы";
    private static final String MOVEMENT = "ввведите путь куда нужно скопировать";
    private static final String ERROR = "ошибка ввода, вывода";
    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<String> nameFiles = new ArrayList<>();
    public static void main(String[] args) {

        System.out.println(WHERE_TO_MOVE);
        Path dir = Paths.get(scanner.nextLine());
        System.out.println(MOVEMENT);
        String newDir = scanner.nextLine();

        transferFile(dir, newDir);
        if (nameFiles.size() != 0){
            statisticsFile();
        }
    }

    /**
     * Метод перемещения файлов
     * @param dir путь откуда нужно переместить файлы
     * @param newDir пуда нужно переместить файлы
     */
    private static void transferFile(Path dir, String newDir) {
        try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)){
            for (Path entry:stream) {
                nameFiles.add(String.valueOf(entry));
                Files.move(entry , Paths.get(newDir + "\\" + entry.getFileName()), StandardCopyOption.REPLACE_EXISTING);
            }
        }catch (IOException e){
            System.out.println(ERROR);
        }
    }

    /**
     * метод который выводит статистику по перемещенным файлам
     */
    private static void statisticsFile() {
        System.out.println("перемещено " + nameFiles.size()+ " Файлов");
        for (String list: nameFiles){
            System.out.println(list);
        }
    }

}
